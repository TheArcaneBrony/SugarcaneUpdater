package net.thearcanebrony.sugarcaneupdater.sourcetype;

import net.thearcanebrony.sugarcaneupdater.Console;

public class Source {
    public String Name = "";
    public String Url = "";

    public Source(String name, String url) {
        Name = name;
        Url = url;
    }

    public void downloadLatest(String file) {
        Console.writeLine(this.getClass().getCanonicalName() + ": Download not implemented!");
    }

    public boolean checkUpdate() {
        Console.writeLine(this.getClass().getCanonicalName() + ": Update checking not implemented!");
        return false;
    }
}
