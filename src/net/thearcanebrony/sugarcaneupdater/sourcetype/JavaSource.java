package net.thearcanebrony.sugarcaneupdater.sourcetype;

import net.thearcanebrony.sugarcaneupdater.*;
import net.thearcanebrony.sugarcaneupdater.ostype.Linux;
import net.thearcanebrony.sugarcaneupdater.ostype.MacOSX;
import net.thearcanebrony.sugarcaneupdater.ostype.OS;
import net.thearcanebrony.sugarcaneupdater.ostype.arch.Arch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static net.thearcanebrony.sugarcaneupdater.DownloadManager.downloadFile;

public class JavaSource extends Source {

    private static final int version = 17;

    public JavaSource(String name, String url) {
        super(name, url);
    }

    @Override
    public void downloadLatest(String outfile) {
        if (!checkUpdate()) {
            Console.writeLine(Name + " is already up to date, not downloading!");
            return;
        }
        String platform = OS.getOS().OsType;
        String arch = Arch.getArch();
        Console.writeLine(String.format("Downloading Temurin Java %s for %s (%s)...", version, platform, arch));
        //do download
        Console.logIO("Getting dir list...");
        FileU.createDirectoryRecursive("tmp");
        FileU.createDirectoryRecursive("java");

        //track relpath
        String serverDir = "";

        downloadFile(String.format(Url + "/%s/%s/%s/dirs.lst", version, platform, arch), "tmp/dirs.lst");
        var dirs = FileU.readLines("tmp/dirs.lst");
        Console.logIO(String.format("Got %s items", dirs.length));
        Console.logIO("Getting file list...");
        downloadFile(String.format(Url + "/%s/%s/%s/files.lst", version, platform, arch), "tmp/files.lst");
        var files = FileU.readLines("tmp/files.lst");
        Console.logIO(String.format("Got %s items", files.length));
        for (var dir : dirs) {
            if (dir.startsWith("./")) {
                serverDir = dir.replace("./", "");
                Console.logDebug(String.format("Found server subdir: %s!", serverDir));
            } else FileU.createDirectoryRecursive("java/" + dir);
        }

        for (var file : files) {
            if (!Files.exists(Paths.get("java/" + file)))
                downloadFile(String.format(Url + "/%s/%s/%s/%s/%s", version, platform, arch, serverDir, file), "java/" + file);
        }

        //make executable
        if (OS.getOS() instanceof Linux || OS.getOS() instanceof MacOSX) {
            Util.startProcess("chmod -v +x java/bin/java");
        }
        Config.JavaBin = "./java/bin/java";
    }

    @Override
    public boolean checkUpdate() {
        if (Runtime.version().feature() >= 17) {
            Config.JavaBin = System.getProperty("java.home") + "/bin/java";
            return false;
        }
        Path javafolder = Paths.get("java");
        try {
            if (javafolder.toFile().exists() && Files.list(javafolder).findFirst().isPresent() && !Config.ForceDownload) {
                Console.writeLine("Java is already downloaded, skipping!");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!Config.LocalState.builds.containsKey(Name)) Config.LocalState.builds.put(Name, new LocalState.Build());
        LocalState.Build b = Config.LocalState.builds.get(Name);
        Console.logDebug(String.format("%s -> %s", b.Number, version));
        if (Config.LocalState.builds.get(Name).Number >= version && Config.LocalState.LatestPull.equals(Name) && !Config.ForceDownload) {
            Console.writeLine(Name + " is already up to date, not downloading!");
            Config.JavaBin = "./java/bin/java";
            return false;
        }
        return true;
    }
}
