package net.thearcanebrony.sugarcaneupdater.ostype;

public class Windows extends OS {
    {
        Name = "Windows";
        OsType = "windows";
        ShellCmd = new String[]{"cmd", "/c"};
    }
}
