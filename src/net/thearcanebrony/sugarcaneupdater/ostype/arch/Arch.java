package net.thearcanebrony.sugarcaneupdater.ostype.arch;

import net.thearcanebrony.sugarcaneupdater.Console;

public class Arch {
    //Available values : x64, x86, x32, ppc64, ppc64le, s390x, aarch64, arm, sparcv9, riscv64
    public static String getArch() {
        var osArch = System.getProperty("os.arch");
        Console.logDebug(String.format("Got OS Arch: %s", osArch));
        switch (osArch) {
            case "amd64":
            case "x64":
                return "x64";
            default:
                Console.writeLine(String.format("Unknown CPU architecture: %s! Please report this!", osArch));
                break;
        }
        return "x64";
    }
}
