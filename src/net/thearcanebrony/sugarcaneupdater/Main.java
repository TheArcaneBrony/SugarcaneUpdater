package net.thearcanebrony.sugarcaneupdater;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] _args) {
        Config.PersistentConfig = PersistentConfig.Get();
        Config.LocalState = Config.PersistentConfig.LocalState;
        ArrayList<String> args = new ArrayList(Arrays.asList(_args));
        ArgManager.init(args);
        ArgManager.preDl(args);
        Config.ServerSource.downloadLatest(Config.OutputFile);
        ArgManager.postDl(args);
        Config.PersistentConfig.Save();
    }
}
