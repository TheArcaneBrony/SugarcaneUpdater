package net.thearcanebrony.sugarcaneupdater;

import net.thearcanebrony.sugarcaneupdater.sourcetype.JenkinsSource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class ArgManager {
    private static final String AikarFlags = "-XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true";
    private static int arg = 0;

    public static void init(ArrayList<String> args) {
        if (args.contains("--verbose")) {
            Config.Verbose = true;
            Console.logDebug("Option: Verbose");
        }
        if (args.contains("--iolog")) {
            Config.Verbose = true;
            Config.LogIO = true;
            Console.logDebug("Option: IOLog");
        }
        if (args.contains("--frd")) {
            Config.ForceDownload = true;
            Console.logDebug("Option: Force Re-Download");
        }
        if (args.contains("--help")) {
            Console.writeLine("SugarcaneUpdater help\n" +
                    "Supported arguments:\n" +
                    " --help            Shows this message\n" +
                    " --unstable        Use dev branch instead of release\n" +
                    " --outfile         Change output filename\n" +
                    " --updateSelf      Update SugarcaneUpdater\n" +
                    " --launch          Launches the server\n" +
                    " --mem             Specify memory to use for server and launch (eg. 2G)\n" +
                    " --prealloc        Pre-allocate ram (eg. 2G) (using --mem pre-allocates that memory by default, unless using this)\n" +
                    " --no-prealloc     Do not pre-allocate ram\n" +
                    " --jvmargs         Specify JVM arguments for server (must be wrapped in \"\")\n" +
                    " --serverargs      Specify server arguments for server (must be wrapped in \"\")\n" +
                    " --no-aikar-flags  Don't use aikar's flags\n" +
                    " --branch          Specify which branch to use (defaults to 1.17/release unless overridden)\n" +
                    " --accept-eula     Accept the Minecraft EULA, saves a step ;)\n" +
                    "\n" +
                    "Debug arguments:\n" +
                    " --verbose         Increase verbosity\n" +
                    " --iolog           Enable I/O logging (implies --verbose)\n" +
                    "\n" +
                    "Internal arguments (NOT FOR END USE! SUPPORT WILL NOT BE PROVIDED):\n" +
                    " --frd             Force re-download\n" +
                    " --loop            Loop mode, used to update cache when a new build is available\n"
            );
            Console.logDebug("Help called, exiting!");
            System.exit(0);
        }
    }

    public static void preDl(ArrayList<String> args) {
        if (args.contains("--updateSelf")) {
            Console.logDebug("Option: Update Self");
            Console.writeLine("You are auto-updating SugarcaneUpdater, bugs may arise!");
            Sources.SUGARCANEUPDATER.downloadLatest("SugarcaneUpdater.jar");
            Config.PersistentConfig.Save();
            args.remove("--updateSelf");
            Util.startProcess(String.format("java -jar SugarcaneUpdater.jar %s", String.join(" ", args)));
            System.exit(0);
        }
        if (args.contains("--unstable")) {
            Console.logDebug("Option: Unstable");
            Console.writeLine("You are downloading from the dev branch, keep in mind this may not be stable and can cause data corruption!");
            Config.ServerSource = Sources.SUGARCANE_DEV;
        }
        if (args.contains("--sysjava")) {
            Console.logDebug("Option: Use system java, implies --skipjava");
            Console.writeLine("WARNING! --sysjava passed! This option is deprecated and no longer does anything!");
        }
        if (args.contains("--skipjava")) {
            Console.logDebug("Option: Don't redownload java");
            Console.writeLine("WARNING! --skipjava passed! This option is deprecated and no longer does anything!");
        }
        if ((arg = args.indexOf("--outfile")) >= 0) {
            String newpath = args.get(arg + 1);
            Console.logDebug("Option: Change output file: " + newpath);
            Config.OutputFile = newpath;
        }
        if ((arg = args.indexOf("--mem")) >= 0) {
            String mem = args.get(arg + 1);
            Console.logDebug("Option: Memory: " + mem);
            Config.LaunchMem = mem;
            Config.LaunchPreMem = mem;
        }
        if ((arg = args.indexOf("--pre-alloc")) >= 0) {
            String mem = args.get(arg + 1);
            Console.logDebug("Option: Pre-allocate: " + mem);
            Config.LaunchPreMem = mem;
        }
        if (args.contains("--no-prealloc")) {
            String str = args.get(arg + 1);
            Console.logDebug("Option: No pre-allocate");
            Console.writeLine("You are not pre-allocating ram. You may be missing out on performance when memory usage increases.");
            Config.LaunchPreMem = "128M";
        }
        if ((arg = args.indexOf("--jvmargs")) >= 0) {
            String str = args.get(arg + 1);
            Console.logDebug("Option: JVM Arguments: " + str);
            Config.JvmArgs = str;
        }
        if ((arg = args.indexOf("--serverargs")) >= 0) {
            String str = args.get(arg + 1);
            Console.logDebug("Option: Server Arguments: " + str);
            Config.ServerArgs = str;
        }
        if (args.contains("--no-aikar-flags")) {
            Console.logDebug("Option: Don't use Aikar's flags");
            Console.writeLine("You are not using Aikar's flags - https://aikar.co/2018/07/02/tuning-the-jvm-g1gc-garbage-collector-flags-for-minecraft/\n" + "Please note that these flags are recommended and help with the performance and ram usage\n" + "If you want the best performance with the lowest RAM usage, please consider re-enabling these flags.");
            Config.AikarFlags = false;
        }
        if ((arg = args.indexOf("--branch")) >= 0) {
            String str = args.get(arg + 1);
            Console.logDebug("Option: Custom branch: " + str);
            if (!str.contains("release"))
                Console.writeLine("You are downloading from a development/prerelease branch!\n" + "Keep in mind this may not be stable and can cause data corruption!");
            Config.ServerSource = new JenkinsSource(String.format("Sugarcane (%s)", str), String.format("https://jenkins.thearcanebrony.net/job/Sugarcane/job/%s/lastStableBuild", URLEncoder.encode(str, Charset.defaultCharset())));
        }
        if (args.contains("--loop")) {
            Console.logDebug("Option: loop mode");
            Console.writeLine("You are running in loop mode! This will cause SugarcaneUpdater to check for updates indefinintely!");
            while (true) {
                if (Config.ServerSource.checkUpdate()) {
                    Console.writeLine("Update available! Downloading...");
                    Config.ServerSource.downloadLatest(Config.OutputFile);
                }
                Util.sleep(60 * 1000);
            }
        }
        if(args.contains("--accept-eula")){
            var file = new File("eula.txt");
            if(file.exists()) file.delete();
            try {
                FileWriter writer = new FileWriter("eula.txt");
                writer.write("#Hereby you declare that Momiji is best awoo!\nEULA=true");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void postDl(ArrayList<String> args) {
        if ((arg = args.indexOf("--launch")) >= 0 || args.contains("--mem")) {
            //download java
            Sources.TAB_JAVA.downloadLatest("");
            String jvmArgs = String.format("%s%s", Config.AikarFlags ? AikarFlags : "", Config.JvmArgs);
            Util.startProcess(String.format("\"%s\" -Xms%s -Xmx%s %s --add-modules=jdk.incubator.vector -jar %s %s", Config.JavaBin, Config.LaunchPreMem, Config.LaunchMem, jvmArgs, Config.OutputFile, Config.ServerArgs));
            //run server
        }
    }
}
