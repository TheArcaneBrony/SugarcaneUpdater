package net.thearcanebrony.sugarcaneupdater;

import net.thearcanebrony.sugarcaneupdater.ostype.OS;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class Util {
    public static void startProcess(String command) {
        ArrayList<String> exec = new ArrayList<>(Arrays.asList(OS.getOS().ShellCmd));
        Console.logDebug(String.format("Detected OS: %s (%s)!", OS.getOS().Name, OS.getOS()));
        Console.logDebug(String.format("Using \"%s\" as shell!", String.join(" ", exec)));
        exec.add(command);
        final ProcessBuilder p = new ProcessBuilder(exec);
        p.inheritIO();
        try {
            Console.writeLine(String.format("Launching command: %s", command));
            p.start().waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    public static String formatSize(long size) {
        return formatSize(size, true);
    }

    public static String formatSize(long size, boolean addSizeUnit) {
        if (size <= 0) return "0";
        if (size <= 1024 || Config.LogIO) return String.format("%s", size) + (addSizeUnit ? " B" : "");
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + (addSizeUnit ? (" " + units[digitGroups]) : "");
    }

    public static String formatSizeProgress(long size, long progress) {
        return String.format("%s/%s (%.2f%%)", formatSize(progress, false), formatSize(size), (double) progress / size * 100);
    }

    public static void sleep(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }
}
