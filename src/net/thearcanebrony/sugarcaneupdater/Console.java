package net.thearcanebrony.sugarcaneupdater;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Console {
    private static final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
    private static int maxSrcLength = 0;
    private static int stackOffset = 2;

    public static void write(String text) {
        String[] lines = text.split("\n");
        StringBuilder textBuilder = new StringBuilder();
        for (String line : lines) {
            textBuilder.append(String.format("[%s]: %s\n", df.format(new Date()), line));
        }
        text = textBuilder.toString();
        System.out.print(text.substring(0, text.length() - 1));
    }

    public static void writeLine(String text) {
        String[] lines = text.split("\n");
        StringBuilder textBuilder = new StringBuilder();
        for (String line : lines) {
            textBuilder.append(String.format("[%s]: %s\n", df.format(new Date()), line));
        }
        text = textBuilder.toString();
        System.out.print(text);
    }

    public static void logDebug(String text) {
        if (Config.Verbose) {
            StackTraceElement ste = Thread.currentThread().getStackTrace()[stackOffset];
            stackOffset = 2;
            String[] lines = text.split("\n");
            StringBuilder source = new StringBuilder(ste.getFileName() + ":" + ste.getLineNumber());
            if (source.length() >= maxSrcLength) maxSrcLength = source.length();
            else while (source.length() < maxSrcLength) source.append(" ");
            StringBuilder textBuilder = new StringBuilder();
            for (String line : lines) {
                textBuilder.append(String.format("%s -> %s\n", source, line));
            }
            text = textBuilder.toString();
            writeLine(text);
        }
    }

    public static void logIO(String text) {
        if (Config.LogIO) {
            stackOffset = 3;
            logDebug(text);
        }
    }
}
