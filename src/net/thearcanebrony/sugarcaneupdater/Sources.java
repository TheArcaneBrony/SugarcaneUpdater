package net.thearcanebrony.sugarcaneupdater;

import net.thearcanebrony.sugarcaneupdater.sourcetype.JavaSource;
import net.thearcanebrony.sugarcaneupdater.sourcetype.JenkinsSource;

public class Sources {
    public static JenkinsSource SUGARCANEUPDATER = new JenkinsSource("SugarcaneUpdater", "https://jenkins.thearcanebrony.net/job/SugarcaneUpdater/lastStableBuild");
    public static JenkinsSource SUGARCANE_RELEASE = new JenkinsSource("Sugarcane", "https://jenkins.thearcanebrony.net/job/Sugarcane/job/1.17%252Frelease/lastStableBuild");
    public static JenkinsSource SUGARCANE_DEV = new JenkinsSource("Sugarcane (DEV)", "https://jenkins.thearcanebrony.net/job/Sugarcane/job/1.19%252Fdev/lastStableBuild");
    public static JavaSource TAB_JAVA = new JavaSource("Java", "https://files.thearcanebrony.net/java");
}
