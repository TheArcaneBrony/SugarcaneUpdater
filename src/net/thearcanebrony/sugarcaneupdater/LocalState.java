package net.thearcanebrony.sugarcaneupdater;

import java.util.HashMap;
import java.util.Map;

public class LocalState {
    public String LatestPull;
    public Map<String, Build> builds = new HashMap<>();

    public static class Build {
        public String Name = "";
        public String OutFile = "";
        public int Number = 0;
    }
}
