package net.thearcanebrony.sugarcaneupdater;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Arrays;

public class DownloadManager {
    private static final DecimalFormat df = new DecimalFormat("#.##");
    private static final String[] Spinner = "|/-\\".split("");
    static boolean isConsole = System.console() != null && !Files.exists(Paths.get("/.dockerenv"));

    static int cooldown = isConsole ? 500 : 1000;
    static String lineSeparator = "              " + (isConsole ? "\r" : "\r\n");
    private static long totalBytes = 0;

    static {
        Console.logDebug("Console: " + isConsole);
    }

    public static String downloadString(String _url) {
        Console.logDebug(String.format("Sending HTTP request to %s!", _url));
        try {
            URL url = new URL(_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            Console.logIO(response.toString());
            return response.toString();
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.writeLine("Something went wrong!\n" + sw);
            return "Something went wrong! " + e;
        }
    }

    public static long downloadFile(String _url, String outfile) {
        long lastUpdateTime = System.currentTimeMillis();
        double updates_per_second = 1000d / cooldown;
        double[] _downloadSpeed = new double[(int) Math.ceil(updates_per_second)];
        int updateCount = 0;
        long lastUpdate = 0;

        //download setup
        long filesize = getFileSize(_url);
        Console.logIO(String.format("Downloading %s bytes from %s...", filesize, _url));
        try {
            totalBytes = 0;
            BufferedInputStream in = new BufferedInputStream(new URL(_url).openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(outfile);
            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, dataBuffer.length)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
                totalBytes += bytesRead;
                if (System.currentTimeMillis() > lastUpdateTime + cooldown && isConsole) {
                    String sp = isConsole ? (Spinner[updateCount % Spinner.length] + " ") : "";
                    _downloadSpeed[(int) (updateCount % updates_per_second)] = totalBytes - lastUpdate;
                    double downloadSpeed = Arrays.stream(_downloadSpeed).sum();
                    Console.write(String.format("%sRead %s at %s/s%s", sp, Util.formatSizeProgress(filesize, totalBytes), Util.formatSize((long) downloadSpeed), lineSeparator));
                    lastUpdate = totalBytes;
                    lastUpdateTime = System.currentTimeMillis();
                    updateCount++;
                }
            }
            fileOutputStream.close();
            in.close();
            Console.writeLine(String.format("Download complete! Downloaded %s into %s!", Config.LogIO ? (totalBytes + " bytes") : Util.formatSize(totalBytes), outfile));
            return totalBytes;
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.writeLine("Something went wrong!\n" + sw);
            return totalBytes;
        }
    }

    private static long getFileSize(String _url) {
        URLConnection conn = null;
        try {
            URL url = new URL(_url);
            conn = url.openConnection();
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).setRequestMethod("HEAD");
            }
            conn.getInputStream();
            return conn.getContentLengthLong();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).disconnect();
            }
        }
    }
}
