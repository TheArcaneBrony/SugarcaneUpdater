package net.thearcanebrony.sugarcaneupdater;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileU {
    public static String readAllText(String path) {
        return Arrays.stream(readLines(path)).collect(Collectors.joining(System.lineSeparator()));
    }

    public static String[] readLines(String path) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line;
            List<String> lines = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            return lines.toArray(new String[0]);
        } catch (IOException e) {
            return new String[]{"An error occurred reading the file!"};
        }
    }

    public static void createDirectoryRecursive(String path) {
        var parts = path.split("/");
        String curPath = path.startsWith("/") ? "/" : "";
        for (var part : parts) {
            curPath += part + "/";
            try {
                Files.createDirectory(Paths.get(curPath));
            } catch (IOException ignored) {
            }
        }
    }
}
