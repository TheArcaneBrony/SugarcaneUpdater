#!/bin/sh
./gradlew runBuild
cd build/libs
rm -rf java

java -jar SugarcaneUpdater.jar --unstable --iolog --mem 2G --accept-eula
cd ../..
